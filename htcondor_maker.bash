#! /bin/bash

function help ()
{
    echo "Usage:"
    echo "htcondor_maker.sh OUTPUT_FILE SHORTNAME EXECUTABLE MEM JOBLEN arg1 arg2 ..."
    echo "OUTPUT_FILE: Full path to the output file."
    echo "SHORTNAME: nickname for job"
    echo "EXEC: executable that will be run"
    echo "MEM: amount of memory requested, in GB"
    echo "JOBLEN: SHORT|LONG. Indicates whether the job is short (less than 24 hrs) or long."
    echo "Long jobs must be registered as suspension jobs. Argument is required."
    exit
}

if [ -z $1 ] || [ $1 == "-h" ] || [ $1 == "--help" ] || [ $1 == "-help" ] ; then 
 help
fi


OUTPUT_FILE=$1 
SHORTNAME=$2 
EXEC=$3 
MEM=$4 
JOBLEN=$5 
ARGS=${@:6}

OUTPUT_DIR=`dirname ${OUTPUT_FILE}`

#print to output file
echo "request_memory   = ${MEM}GB " > ${OUTPUT_FILE}
echo "Universe         = vanilla " >> ${OUTPUT_FILE}
echo "Log              = "${OUTPUT_DIR}'/'${SHORTNAME}'.log ' >> ${OUTPUT_FILE}
echo "Output           = "${OUTPUT_DIR}'/'${SHORTNAME}'.out ' >> ${OUTPUT_FILE}
echo "Error            = "${OUTPUT_DIR}'/'${SHORTNAME}'.err ' >> ${OUTPUT_FILE}
echo "Executable       = "${EXEC}" " >> ${OUTPUT_FILE}

if [ ${JOBLEN} == "SHORT" ] ; then
    echo "+MaxExecutionTime = 86400 " >> ${OUTPUT_FILE}
elif [ ${JOBLEN} == "LONG" ]; then
    echo "+MaxExecutionTime = 129600 " >> ${OUTPUT_FILE}
else
    echo "Incorrect arguments to script!"
    help
fi


echo " " >> ${OUTPUT_FILE}
echo "Arguments        = "${ARGS}" " >> ${OUTPUT_FILE}
echo "Queue " >> ${OUTPUT_FILE}


