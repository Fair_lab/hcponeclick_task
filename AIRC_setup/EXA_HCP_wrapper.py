#! /usr/global/bin/python
import os
import sys
import pwd
import argparse
import subprocess
import shutil
import paramiko
#@todo fix hack to get root dir on python path. See http://blog.habnab.it/blog/2013/07/21/python-packages-and-you/
#turns out executables shouldn't really be in sub-packages
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from util import UtilityBelt

prog_descrip = """setup script for HCP on exacloud. Calls HCP prep for nifti conversion and
                slicetime correction, then backgrounds and nohups exacloud_transfer.py"""

exa_transfer_path = os.path.join(os.path.dirname(os.path.realpath(__file__)),'exacloud_transfer.py')


def main():

    arg_parser = get_parser()
 

    args = arg_parser.parse_args()
    S = args.subject_visit
    subject_id = S.split('/')[0]

    # Run HCP dicom prepare

    hcp_dicom_prep_path = UtilityBelt().get_path('AIRC','dicom_prep')
    prep_cmdlist = [hcp_dicom_prep_path, '-d', args.dicom_dir, '-s', args.subject_visit, '-j',args.json_path]

    
    subprocess.call(prep_cmdlist)

    launch_rsync(subject_id, args.dicom_dir)

def launch_rsync(subject_id, sorted_dir):
    """launch and nohup rsync script on beast"""
    #rename pipeline output dir to EXA_HCP
    hcp_share = sorted_dir.replace('sorted','processed')
    util = UtilityBelt()
#########################################
#    old_pipeline_name = util.get_config_val('old_pipeline_name')
#    new_pipeline_name = util.get_config_val('new_pipeline_name')
#
#    pipe_dir = os.path.join(hcp_share, old_pipeline_name)
#    new_pipe = os.path.join(hcp_share, new_pipeline_name)
#    if os.path.exists(new_pipe):
#        shutil.rmtree(new_pipe)
#
#    os.rename(pipe_dir, new_pipe)
#
#    output_dir = os.path.join(new_pipe, 'unprocessed')
#    rsync_log_path = os.path.join(new_pipe, 'exacloud_rsync_log.txt')
################ Removed old/new pipeline and replaced with just pipeline - Anders 11/1/2016
    pipeline_name = util.get_config_val('pipeline_name') #edit config.json accordingly
    pipe_dir = os.path.join(hcp_share, pipeline_name)
#    if os.path.exists(pipe_dir):
#        shutil.rmtree(pipe_dir)
    output_dir = os.path.join(pipe_dir, 'unprocessed')
    rsync_log_path = os.path.join(pipe_dir, 'exacloud_rsync_log.txt')
################

    usr = pwd.getpwuid(os.getuid()).pw_name #safer way to get username in queuing system
    cmd = 'ssh -o StrictHostKeyChecking=no -o LogLevel=quiet {}@beast "nohup {} -s {} -o {} &>>{} &"'
    cmd = cmd.format(usr, exa_transfer_path,subject_id,output_dir, rsync_log_path)
    
    print "starting exacloud handoff:"
    print cmd 
    subprocess.call(cmd, shell=True)

def get_parser():

    arg_parser = argparse.ArgumentParser(description=prog_descrip)

    arg_parser.add_argument('-d', metavar='DICOM_DIR', nargs='?', action='store', required=True, type=os.path.abspath,
                            help=('Directory where dicoms are stored.'),
                            dest='dicom_dir'
                           )
    arg_parser.add_argument('-j', metavar='JSON_PATH', nargs='?', action='store', required=True, type=os.path.abspath,
                            help=('Path to subject(s) json file'),
                            dest='json_path'
                           )
    arg_parser.add_argument('-s', metavar='SUBJECT_VISIT', nargs='?', action='store', required=True,
                            help=('Subject VISIT'),
                            dest='subject_visit'
                           )
    return arg_parser


if __name__ == "__main__":
    main()   
