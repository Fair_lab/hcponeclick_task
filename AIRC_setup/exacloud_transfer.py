#! /usr/global/bin/python
import os
import sys
import argparse
import subprocess
import json
import time
import fcntl
import errno
from shutil import rmtree
#@todo fix hack to get root dir on python path. See http://blog.habnab.it/blog/2013/07/21/python-packages-and-you/
#turns out executables shouldn't really be in sub-packages
sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.realpath(__file__))))
from util import UtilityBelt


prog_descrip = """rsyncs niftis to exacloud and kicks off processing on exacloud. Has locking mechanism to ensure
               that only one rsync runs on beast at a time."""

def main():
    
    arg_parser = get_parser()

    args = arg_parser.parse_args()


    lock_fd = get_lock()
    
    try:
        rsync_files(args.output_dir, args.subject_id)
    except Exception as e:
        print "rsync to exacloud failed:"
        print e
    
    release_lock(lock_fd)


    launch_oneclick(args.output_dir, args.subject_id)

    # delete unprocessed data from /scratch
    rmtree(args.output_dir)


def get_parser():

    arg_parser = argparse.ArgumentParser(description=prog_descrip)

    arg_parser.add_argument('-o', metavar='OUTPUT_DIR', nargs='?', action='store', required=True, type=os.path.abspath,
                            help=('Directory where niftis should be created (if not the default).'),
                            dest='output_dir')
    arg_parser.add_argument('-s', metavar='SUBJECT_ID', nargs='?', action='store', required=True, type=str,
                            help=('SubjectID string to name the NIfTIs.'),
                            dest='subject_id')

    return arg_parser




def get_lock():
    """ acquire file lock, and return file object once acquired

    Returns:
        file: file object for rsync_lock.txt, the file lock

    This step is necessary to ensure that hcponeclick as a whole (all users) 
    only allows one rsync process to occur at once, so that bandwidth
    usage stays at acceptable levels on beast.

    Code inspired by http://tilde.town/~cristo/file-locking-in-python.html
    """

    this_dir = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
    lock_path = os.path.join(this_dir, 'rsync_lock.txt')

    while True:
        try:
            fd = open(lock_path, 'w')
            fcntl.flock(fd, fcntl.LOCK_EX | fcntl.LOCK_NB)
            print "lock acquired!"
            return fd
        except IOError as e:
            # raise on unrelated IOErrors
            if e.errno != errno.EAGAIN:
                raise
            print "lock unavailable"
            time.sleep(10)

def release_lock(fd):
    print "releasing lock"
    fcntl.flock(fd, fcntl.LOCK_UN)
    fd.close()

def get_exa_usr():

    config_path = os.path.expanduser('~/.hcp.config')
    config_f = open(config_path)
    config = json.load(config_f)
    config_f.close()
    return config['exa_uname']
    

def get_exa_HCP_path(exa_usr):
    return UtilityBelt().get_path('Exacloud', 'data_dir')


def rsync_files(unprocessed_dir,subject_id):
    exa_usr = get_exa_usr()
    exa_lustre_HCP_path = get_exa_HCP_path(exa_usr)

    subprocess.call('ssh -o StrictHostKeyChecking=no {}@exacloud "mkdir -p {}"'.format(exa_usr, exa_lustre_HCP_path), shell=True)

    #add './' for rsync -R (relative path) command. See rsync man page for details.
    HCP_index = unprocessed_dir.find('processed/')
    from_path = '{}./{}'.format(unprocessed_dir[:HCP_index],unprocessed_dir[HCP_index:])

    #perform rsync
    rsync_cmd = 'rsync -e "ssh -o StrictHostKeyChecking=no" -Rrl --bwlimit=10240 {} {}@exacloud:{}'.format(from_path,exa_usr,exa_lustre_HCP_path)
    print rsync_cmd

    subprocess.call(rsync_cmd, shell=True)

def launch_oneclick(unprocessed_dir,subject_id):
    usr = get_exa_usr()

    HCP_index = unprocessed_dir.find('processed/')
    proc_to_unproc_path = unprocessed_dir[HCP_index:]
    proc_to_pipe_path = os.path.dirname(proc_to_unproc_path)

    exa_subj_dir = os.path.join(get_exa_HCP_path(usr), proc_to_pipe_path)
    
    oneclick_exa_path = UtilityBelt().get_path('Exacloud', 'run_oneclick')
    cmd = 'ssh -o StrictHostKeyChecking=no {}@exacloud "{} -s {} -id {} -clean_run "'.format(usr, oneclick_exa_path, exa_subj_dir, subject_id)
    print cmd
    subprocess.call(cmd, shell=True)

if __name__ == "__main__":
    main()
