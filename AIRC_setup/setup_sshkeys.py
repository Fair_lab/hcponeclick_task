#! /usr/global/bin/python
import os
import sys
import subprocess
import time
import json
import argparse
import paramiko

"""
   Inspired by https://29a.ch/2010/9/8/deploy-ssh-public-key-multiple-servers-python-paramiko
"""
prog_descrip = """SSH key setup script for Exa HCP (a.k.a 'hcponeclick'). Has action arguments for
                creating the hcp.config user file, creating ssh keys to and from exacloud, and
                testing ssh key setup."""


def main():

    arg_parser = get_parser()

    args = arg_parser.parse_args()

    if args.action == 'test_keys':
        if both_keys_valid(args.exa_uname):
            print "SSH keys are valid"
            sys.exit(0)
        else:
            print "SSH keys are invalid"
            sys.exit(1)
    elif args.action == 'create_config':
        create_config(args.exa_uname)
        sys.exit(0)
    elif args.action == 'create_keys':
        if args.exa_uname and args.exa_pass and args.airc_pass:
            setup_keys(args.exa_uname, args.exa_pass, args.airc_pass)
        else:
            print "not enough arguments to create ssh keys. See setup_sshkeys.py --help"
            sys.exit(1)
    else:
        print "invalid action"
        sys.exit(1)

def get_parser():

    arg_parser = argparse.ArgumentParser(description=prog_descrip)
    arg_parser.add_argument('--action', metavar='ACTION', nargs='?',
                            action='store', required=False, type=str, default='test_keys',
                            help=('action to perform. Options: test_keys, create_keys, create_config'), dest='action')

    arg_parser.add_argument('--exa_uname', metavar='EXACLOUD_USERNAME', nargs='?',
                            action='store', required=True, type=str, default=None,
                            help=('exacloud username for user'), dest='exa_uname')

    arg_parser.add_argument('--exa_pass', metavar='EXACLOUD_PASSWORD', nargs='?',
                            action='store', required=False, type=str, default=None,
                            help=('exacloud password for user. Required if action is create_keys'), dest='exa_pass')

    arg_parser.add_argument('--airc_pass', metavar='AIRC_PASSWORD', nargs='?',
                            action='store', required=False, type=str, default=None,
                            help=('AIRC password for user. Required if action is create_keys'), dest='airc_pass')

    return arg_parser


def create_config(exa_uname):
    # write username to config
    config_dict = {'exa_uname': exa_uname}
    config_path = os.path.expanduser('~/.hcp.config')
    f = open(config_path, 'w')
    json.dump(config_dict, f)
    f.close()


def setup_keys(exa_uname, exa_pass, airc_pass):
    if both_keys_valid(exa_uname):
        print "Keys already set up correctly. Doing nothing."
        sys.exit(0)
    
    airc_uname = os.getlogin()
    try:
        deploy_key('exacloud', exa_uname, exa_pass, 'beast', airc_uname, airc_pass)
        deploy_key('beast', airc_uname, airc_pass, 'exacloud', exa_uname, exa_pass)
    except Exception as e:
        print "ssh key generation failed"
        print "error details:"
        print e
        sys.exit(1)

    if not both_keys_valid(exa_uname):
        sys.exit(1)

    create_config(exa_uname)


def deploy_key(server_hostname, server_uname, server_pass, client_hostname, client_uname, client_pass):
    #connect to the host you want to generate the key on
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(client_hostname, username=client_uname, password=client_pass)
    rsa_key_path = '${HOME}/.ssh/id_rsa.pub'
    rsa_key_path_name = '${HOME}/.ssh/id_rsa'
    stdin, stdout, stderr = client.exec_command('ls {}'.format(rsa_key_path))
    if stderr.read():
        print 'generating key'
        execute_synchronous_cmd(client, 'ssh-keygen -b 2048 -t rsa -f {} -q -N ""'.format(rsa_key_path_name))

    stdin, stdout, stderr = client.exec_command('cat ${HOME}/.ssh/id_rsa.pub')
    key = stdout.read()
    print 'key is {}'.format(key)
    client.close()

    if not key:
        print "There was an error generating your ssh key. Please try again."
        return
    #connect to the host you want to deploy the public key to
    server = paramiko.SSHClient()
    server.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    server.connect(server_hostname, username=server_uname, password=server_pass)
    execute_synchronous_cmd(server, 'mkdir -p ~/.ssh/')
    execute_synchronous_cmd(server, 'echo "{}" >> ~/.ssh/authorized_keys'.format(key))
    execute_synchronous_cmd(server, 'chmod 644 ~/.ssh/authorized_keys')
    execute_synchronous_cmd(server, 'chmod 700 ~/.ssh/')
    #execute_synchronous_cmd(server, 'chmod go-w ~')
    server.close()

def execute_synchronous_cmd(server,cmd):
    """synchronously execute a command on the a remote server



    Args:
        server (SSHClient): paramiko ssh client connected to desired host
        cmd (str): Command to execute on the server

    Returns:
        stdin,stdout,stderr (channel): paramiko channels returned from exec_command

    """
    stdin, stdout, stderr = server.exec_command(cmd)
    stdout.channel.recv_exit_status()
    return stdin, stdout, stderr

def both_keys_valid(exa_uname):
    
    if valid_key_setup('exacloud', exa_uname) and valid_key_setup_from_exacloud(exa_uname):
        return True

    return False

def valid_key_setup(server, server_uname):
    print "Testing airc to {} setup".format(server)
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    try:
        client.connect(server, username=server_uname)
    except paramiko.ssh_exception.SSHException:
        print "beast to exacloud failed"
        return False
    else:
        client.close()
        return True


def valid_key_setup_from_exacloud(exa_uname):

    print "Testing exacloud to airc setup"
    airc_uname = os.getlogin()
    exa_client = paramiko.SSHClient()
    exa_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    exa_client.connect('exacloud', username=exa_uname)

    cmd = 'ssh -o StrictHostKeyChecking=no -o LogLevel=quiet {}@beast "echo test"'
    stdin, stdout, stderr = exa_client.exec_command(cmd.format(airc_uname))
    error = stderr.read()
    print error
    if error:
        return False

    exa_client.close()
    return True

if __name__ == "__main__":
    main()
