import json
import os

from Exceptions import ProtocolNotFoundException

class UtilityBelt(object):
    """
    Utility belt for hcponeclick.

    Interacts with external resources, such as the config file and os peculiarities.
    """
    code_dir = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
 
    def __init__(self):
        self._config = None

    @property
    def config(self):

        if not self._config:
            f = open(os.path.join(self.code_dir,'config.json'))
            self._config = json.load(f)
            f.close()

        return self._config


    def get_protocol(self, subj_pipeline_path):
        """
            unprocessed_path: .../study/subject/visit/pipeline
        """
        
        split_path = self.strip_trailing_slash(subj_pipeline_path).split('/')

        study = split_path[-4]

        study_protocol_dict = self.config['study_protocol_map']

        if not study in study_protocol_dict.keys():
            raise ProtocolNotFoundException(study)

        return os.path.join(self.code_dir,"protocols",study_protocol_dict[study])

    @staticmethod
    def strip_trailing_slash(path):

        if path[-1] == '/':
            path = path[:-1]

        return path

    def get_path(self, server, pathtype):
        
        try:
            return str(self.config[server + '_paths'][pathtype])
        except KeyError:
            print "pathtype '{}'' not found in config".format(pathtype)
            raise

    def get_config_val(self, config_key):
        try:
            return str(self.config[config_key])
        except KeyError:
            print "key '{}'' not found in config".format(config_key)
            raise
