#!/home/exacloud/lustre1/fnl_lab/code/bin/anaconda2/bin/python

import argparse
import os
from Nodes import NodeManager


def main():

    parser = get_parser()

    args = parser.parse_args()

    #@todo input validation
    subject_dir = args.subject_dir
    id = args.id
    node_name = args.node_name

    node_manager = NodeManager(id, subject_dir)
    node_manager.run_node(node_name)


def get_parser():

    parser = argparse.ArgumentParser(description="HCP one click node runner")
    parser.add_argument('-s', metavar='SUBJECT_DIR', action='store', required=True, type=os.path.abspath,
                            help='Path to subject\'s processed pipeline dir. Example: HCP/processed/mystudy/mysubj/myvis/HCPpipe/',
                            dest='subject_dir')
    parser.add_argument('-id','--id', metavar='ID', action='store', required=True, type=str,
                        help='subject ID',
                        dest='id')
    parser.add_argument('-node', '--node', metavar='NODE', action='store', type=str, required=True,
                        help='Step of the pipeline to run. Options: HcpPre, HcpFree, HcpPost, HcpVol, HcpSurf, FNLpreproc',
                        dest='node_name')
    return parser

if __name__ == "__main__":
    main()
