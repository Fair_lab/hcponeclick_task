#!/home/exacloud/lustre1/fnl_lab/code/bin/anaconda2/bin/python

from Nodes import NodeManager
import os, sys, argparse
from shutil import rmtree
from util import UtilityBelt
pipeline_name = 'hcponeclick_task'

def main():
    parser = get_parser()

    args = parser.parse_args()

    #@todo input validation

    subject_dir = args.subject_dir
    id = args.id

    
    if not os.path.exists(subject_dir):
        print "subject path does not exist. Please check your path and try again"
        sys.exit()

    oneclick_path = '{}/hcponeclick_task'.format(subject_dir)
    hcp_output_dir = '{}/{}'.format(subject_dir,id)

    #start fresh
    if args.clean:
        if os.path.exists(oneclick_path):
            rmtree(oneclick_path)
        if os.path.exists(hcp_output_dir):
            rmtree(hcp_output_dir)

    if not os.path.exists(oneclick_path):
        os.mkdir(oneclick_path)

    #kick off the process
    manager = NodeManager(id, subject_dir)
    manager.queue_node(manager.first_node_name())

def get_parser():

    parser = argparse.ArgumentParser(description="HCP one click pipeline runner")
    parser.add_argument('-s', metavar='SUBJECT_DIR', action='store', required=True, type=os.path.abspath,
                            help='Path to subject\'s processed pipeline dir. Example: HCP/processed/mystudy/mysubj/myvis/HCPpipe',
                            dest='subject_dir')
    parser.add_argument('-id', '--id', metavar='ID', action='store', required=True, type=str,
                        help='subject ID',
                        dest='id')
    parser.add_argument('-clean_run','--clean_run',  action='store_true',
                        help='remove previous runs of the pipeline and "start fresh"',
                        dest='clean')
    return parser

if __name__ == "__main__":
    main()
