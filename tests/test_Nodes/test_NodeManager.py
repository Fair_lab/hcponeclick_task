from nose.tools import *
from mock import Mock, MagicMock
from Nodes import NodeManager, NodeStep

#Impure unit test for checking that the email is sent. Remove the 
#decorator if you want to test email sending.
@nottest
def test_send_mail():
    node_mngr = NodeManager("test_ID", "/test/path/")
    node_mngr.mail("test msg")

def test_send_success():
    success_msg =  "Processing succeeded!\nYou can find your data for subject {} at:\n{}".format("test_ID", "/test/path/")
    node_mngr = NodeManager("test_ID", "/test/path/")
    node_mngr.mail = MagicMock(name='mail')
    test_node = NodeStep("test_ID", "/test/path/")
    node_mngr.send_success(test_node)
    node_mngr.mail.assert_called_with(success_msg)

def test_send_failure():
    failure_msg = "Processing failed for subject {} during node step {}\n\n".format("test_ID", "NodeStep")
    node_mngr = NodeManager("test_ID", "/test/path/")
    node_mngr.mail = MagicMock(name='mail')
    test_node = NodeStep("test_ID", "/test/path/")

    node_mngr.send_failure(test_node)
    node_mngr.mail.assert_called_with(failure_msg)

def test_send_failure_with_details():
    failure_details = "Example failure details"
    failure_msg = "Processing failed for subject {} during node step {}\n\nFailure Details:\n{}".format("test_ID", "NodeStep", failure_details)
    
    node_mngr = NodeManager("test_ID", "/test/path/")
    node_mngr.mail = MagicMock(name='mail')
    
    test_node = NodeStep("test_ID", "/test/path/")
    test_node.get_failure_comment = MagicMock(name='get_failure_comment')
    test_node.get_failure_comment.return_value = failure_details

    node_mngr.send_failure(test_node)
    node_mngr.mail.assert_called_with(failure_msg)

def test_nodes_getter():
    node_mngr = NodeManager("test_ID", "/test/path/")
    assert node_mngr.nodes[0].get_classname, 'HcpGenericWrapper'
