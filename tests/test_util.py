from nose.tools import *
from mock import Mock, MagicMock
from util import UtilityBelt
from Exceptions import ProtocolNotFoundException



def test_load_config_config_not_empty():

    util = UtilityBelt()
    assert not util.config == None


def test_get_protocol_protocol_found():

    util = UtilityBelt()

    test_unproc_path = '/HCP/processed/ADHD-HumanYouth-OHSU/test_subj/test_vis/pipeline'
    prot = util.get_protocol(test_unproc_path)

    assert prot == 'ADHD_protocol.sh'


@raises(ProtocolNotFoundException)
def test_get_protocol_protocol_not_found():

    util = UtilityBelt()
    util.get_protocol('/HCP/processed/Fake_Study_Unicorns/test_subj/test_vis/pipeline')


def test_strip_trailing_slash():

    util = UtilityBelt()
    path = '/my/test/path/'

    stripped_path = util.strip_trailing_slash(path)

    assert path[:-1] == stripped_path
