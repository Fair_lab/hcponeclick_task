#!/bin/bash
#
# Combines settings for your protocol with the system's SetUpHCPPipeline.sh EnvironmentScript.  This script must be executable
#

export QUEUE="HTCondor" #Available queuing system options are "SGE" or "HTCondor" or "localhost"

##### HCP PIPELINE DIRECTORY AND SETUP (DO NOT CHANGE THE ORDER OF THESE TWO LINES) #####
export HCPPIPEDIR="/home/exacloud/lustre1/fnl_lab/code/internal/pipelines/HCP_release_20161027" #location of the github-cloned repository of the HCP pipeline
. /home/exacloud/lustre1/fnl_lab/code/internal/pipelines/HCP_generic/SetUpHCPPipeline_Exacloud_UseHCPDependencies.sh # the ". " in front is very important to source the SetUp file for parent scripts.  Do not remove those two characters from the front.

##### ATLAS SETTINGS #####

# MNI 0.7mm T1w template
export T1Atlas="${HCPPIPEDIR_Templates}/MNI152_T1_1mm.nii.gz"
# Brain extracted MNI 0.7mm T1w template
export T1AtlasBrain="${HCPPIPEDIR_Templates}/MNI152_T1_1mm_brain.nii.gz"
# MNI 2mm T1w template
export T1AtlasResamp="${HCPPIPEDIR_Templates}/MNI152_T1_2mm.nii.gz"
# MNI 0.7mm T2w Template
export T2Atlas="${HCPPIPEDIR_Templates}/MNI152_T2_1mm.nii.gz"
# Brain extracted MNI 0.7mm T2w Template
export T2AtlasBrain="${HCPPIPEDIR_Templates}/MNI152_T2_1mm_brain.nii.gz"
# MNI 2mm T2w Template
export T2AtlasResamp="${HCPPIPEDIR_Templates}/MNI152_T2_2mm.nii.gz"
# Brain mask MNI 0.7mm template
export AtlasMask="${HCPPIPEDIR_Templates}/MNI152_T1_1mm_brain_mask.nii.gz"
# Resampled brain mask MNI 2mm template
export AtlasResampMask="${HCPPIPEDIR_Templates}/MNI152_T1_2mm_brain_mask_dil.nii.gz"

##### PROTOCOL SETTINGS #####

# CorrectionMethod (Readout Distortion Correction):
#
#   Currently supported Averaging and readout distortion correction 
#   methods: (i.e. supported values for the AvgrdcSTRING variable in this
#   script and the --avgrdcmethod= command line option for the 
#   PreFreeSurferPipeline.sh script.)
#
#   "NONE"
#     Average any repeats but do no readout distortion correction
#
#   "FIELDMAP"
#     This value is equivalent to the "SiemensFieldMap" value described
#     below. Use of the "SiemensFieldMap" value is prefered, but 
#     "FIELDMAP" is included for backward compatibility with the versions
#     of these scripts that only supported use of Siemens-specific 
#     Gradient Echo Field Maps and did not support Gradient Echo Field 
#     Maps from any other scanner vendor.
#
#   "TOPUP"
#     Average any repeats and use Spin Echo Field Maps for readout 
#     distortion correction

export CorrectionMethod="FIELDMAP"

# delta TE in ms for gradient echo field map
# The TE variable should be set to 2.46ms for 3T scanner, 1.02ms for 7T scanner or "NONE" if not using.
# Looking at a field map, echotimes are 5.19ms and 7.65ms, so 7.65-5.19=2.46, this is the SIEMENS 3T standard

export FieldmapDeltaTE="2.46"

# Topup config file path if using TOPUP, set to NONE if using regular gradient echo FIELDMAP
# "${HCPPIPEDIR_Config}/b02b0.cnf"

export TopUpConfigFile="NONE"

# A bit of a misnomer, this is the unwarping direction to use for the FIELDMAP or TOPUP options
# x or y or z or x- or y- or z- or "NONE" if not used

export TopupUnwarpDir="y"

# Echo Spacing or Dwelltime of spin echo EPI MRI image. Specified in seconds.
# for use with TOPUP or FIELDMAP. Set to "NONE" if not used. 
# 
# The EPI Dwell Time can be calculated from:
# TopupDwellTime = 1/(BandwidthPerPixelPhaseEncode * # of phase encoding samples)
# DICOM field (0019,1028) = BandwidthPerPixelPhaseEncode
# DICOM field (0051,100b) = AcquisitionMatrixText first value (# of phase encoding samples)
# On Siemens, iPAT/GRAPPA factors have already been accounted for.  
# 
# example from an EPI DICOM's dcmdump command:
# (0019,1028) FD 31.25                                    #   8, 1 Unknown Tag & Data
# (0051,100b) LO [64*64]                                  #   6, 1 Unknown Tag & Data
# so the calculation would be: 1/(31.25*64) = 0.0005
# 
# Here is a helpful online reference in understanding which timing is which:
# http://support.brainvoyager.com/functional-analysis-preparation/27-pre-processing/459-epi-distortion-correction-echo-spacing.html

export TopupDwellTime="FILL_IN_THE_BLANK"

# The T1 sample spacing can be calculated as such:
# For General Electric scanners, 1/((0018,0095)*(0028,0010))
# for SIEMENS use DICOM field (0019,1018) in seconds
# for use with FIELDMAP or TOPUP or "NONE" if not used
# 
# example from a T1 DICOM's dcmdump command:
# (0019,1018) IS [10900]                                  #   6, 1 Unknown Tag & Data
# this value appears in nanoseconds so the calculation would be: 10900/1000000000 = 0.0000109

export T1Spacing="FILL_IN_THE_BLANK"

# The T2 sample spacing can be calculated as such:
# For General Electric scanners, 1/((0018,0095)*(0028,0010))
# for SIEMENS use DICOM field (0019,1018) in seconds
# for use with FIELDMAP or TOPUP or "NONE" if not used
# 
# example from a T2 DICOM's dcmdump command:
# (0019,1018) IS [3400]                                   #   4, 1 Unknown Tag & Data
# this value appears in nanoseconds so the calculation would be: 3400/1000000000 = 0.0000034

export T2Spacing="FILL_IN_THE_BLANK"

# BrainSize in mm, 150 for humans
# OMD: z direction (top), used to remove neck

export FOVSize="150"

# Target final resolution of fMRI data. 2mm is recommended for 3T HCP data, 1.6mm for 7T HCP data (i.e. should match acquired resolution).  Use 2.0 or 1.0 to avoid standard FSL templates

export OutputFMRIResolution="2.0"

# 2mm if using HCP minimal preprocessing pipeline outputs
# Usually 2mm, if multiple delimit with @, must already exist in templates dir
export GrayordinatesRes="2"

# Usually "164", as in 164k vertices

export HighResMeshKvertices="164"

# Usually "32", as in 32k vertices, if multiple delimit with @, must already exist in templates dir

export LowResMeshesKvertices="32"

# Recommended to be roughly the voxel size (OutputFMRIResolution)

export SmoothingSize="2.0"

##### Additional required variables #####

# true or false
# NOTE: the jacobian option only applies the jacobian of the distortion corrections to the fMRI data, and NOT from the nonlinear T1 to template registration
# NOTE: currently is only used in gradient distortion correction of spin echo fieldmaps to topup
# not currently in usage, either, because of this very limited use
export UseJacobian="false"

# MCFLIRT or FLIRT, MCFLIRT is recommended
# use = "FLIRT" to run FLIRT-based mcflirt_acc.sh, or "MCFLIRT" to run MCFLIRT-based mcflirt.sh

export MotionCorrectionType="MCFLIRT"

# NONE, LEGACY, or SEBASED: LEGACY uses the T1w bias field, SEBASED calculates bias field from spin echo images (which requires TOPUP distortion correction)

export BiasCorrection="NONE"

# true or false, whether or not you are using a T2w_SPC image

export useT2="true"

