"""
    Collection of the exceptions used in this project
"""


class NodeNotFoundException(Exception):

    def __init__(self, nodestr):
        self.nodestr = nodestr

    def __str__(self):
        return "There is no node type defined in the pipeline for name " + str(self.nodestr)

class ProtocolNotFoundException(Exception):

    def __init__(self, study):
        self.study = study

    def __str__(self):
        return  """There is no protocol configured for study '{}'. 
                Please create a protocol in the hcponeclick/protocols/ directory, 
                and map {} to this protocol in the config.json (see FairLab member for assistance)""".format(self.study, self.study)
