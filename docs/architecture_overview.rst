Architecture Overview
**********************

Big Picture
===========

This project has three main parts:

#. Setup scripts that run on the AIRC and interact with the 
   PATHGUI to allow for selecting data and transferring it to exacloud.
#. Code that runs on exacloud to handle queuing and verifying each node step.
#. Integrations with the PATH_GUI, HCPgeneric, FNL_preproc, WashU's HCP release, and other small utilities.

The hcponeclick codebase maintains the first two sets of scripts, so the codebase must be deployed on
both exacloud and the AIRC. The PATHGUI must be deployed on the AIRC, while HCPgeneric, FNL_preproc, and 
WashU's HCP release should be deployed on exacloud.

Pipeline overview:

.. image:: images/HcpPipe.png

AIRC Setup via PATH_GUI
=======================

The PATH_GUI launches processing on the AIRC. First, the PATH_GUI calls setup_sshkeys.py to validate
and/or create ssh keys and the user's ~/.hcp.config, which contains their exacloud username. Then,
the PATH_GUI qsubs EXA_HCP_wrapper.py, which runs HCP Prep. On completion, EXA_HCP backgrounds and nohups
exacloud_transfer.py, which transfers data to exacloud and launches the pipeline on exacloud.

.. image:: images/AIRC_EXA_HCP.png

Exacloud Code
=============

The pipeline on Exacloud is kicked off by exacloud_transfer.py after the data transfer completes.
run_oneclick.py creates the bookkeeping directory and contacts the NodeManager to queue the first node.
The NodeManager coordinates with each NodeStep to handle queuing run_node.py and how to handle node 
failures (rerun node, send success email, or send failure email). NodeStep is the parent class for each
of the pipeline's node implementations. These nodes handle updating the bookkeeping directory, validation
of each step, calling HCP Generic or the WashU HCP release, and queuing details.

.. image:: images/exacloud_architecture.png

Output Locations
================

#. AIRC: data is output to /scratch/FAIR_HCP/HCP/processed/STUDY/SUBJ/VIS/PIPELINE
#. Exacloud:

    a. Data Directory: :code:`/home/exacloud/lustre1/fnl_lab/data/HCP/processed/STUDY/SUBJ/VIS/PIPELINE/SUBJ`
    b. Bookkeeping directory: :code:`/home/exacloud/lustre1/fnl_lab/data/HCP/processed/STUDY/SUBJ/VIS/PIPELINE/hcponeclick/NODE_NAME`
       bookkeeping directory contains a status.json file which keeps track of the node's status, and the HTCONDER 
       error, output, and log files for each step

