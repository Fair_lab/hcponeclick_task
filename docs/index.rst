.. Hcp One Click documentation master file, created by
   sphinx-quickstart on Tue Mar  8 19:24:30 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Hcp One Click's documentation!
=========================================

Contents:

.. toctree::
   :maxdepth: 1

   quickstart
   configuration
   architecture_overview
   exacloud_scripts
   AIRC_setup
   Nodes


Project Overview
================
The goal of this project is to make it possible for AIRC users to run the Human Connectome Project
minimal preprocessing pipeline and FNL_preproc on exacloud. They should be able to do this with minimal knowledge
of Exacloud, the HCP pipeline, and data transfers.

For more information on the pipeline, see: http://www.ncbi.nlm.nih.gov/pubmed/23668970

For more information on FNL_preproc, see: https://everett.ohsu.edu/wiki/FNL_preproc


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

