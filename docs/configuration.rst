Configuration
*************

config.json
===========
The config.json keeps track of which protocols can be used for which studies, 
where dependencies are located on both the AIRC and exacloud. This file is not committed to 
version control, so it will be easy to update paths as dependencies move around without having
to worry about how this fits in to application versioning.

See instructions for creating config.json in the template file, config_template.json.

.. _new-study-instructions:

Adding a New Study
==================
Several things need to happen when a new study is added. Go through each of the categories
below to ensure studies are set up correctly.

1. Add study to PATH_GUI study dropdown
---------------------------------------
the study names are pulled from :code:`/scratch/FAIR_HCP/HCP/sorted/`. If you don't see your
study in the dropdown list, follow these instructions to create a new study::
    
    # replace MY_STUDY_NAME with whatever text you want to label your study
    mkdir /scratch/FAIR_HCP/HCP/sorted/MY_STUDY_NAME
    chmod ug+rwx /scratch/FAIR_HCP/HCP/sorted/MY_STUDY_NAME

2. Create protocol settings File
--------------------------------
The protocols folder holds scan configuration protocols needed for running HCPgeneric. 
Configuring these protocols can be difficult, so see Eric Earl (earl@ohsu.edu),
Shannon Buckley(bucklesh@ohsu.edu) or Oscar Miranda Dominguez (mirandad@ohsu.edu) for 
assistance with creating protocols for new studies.

Currently, new protocols should be saved to the hcponeclick/protocols/ folder and committed
to version control.

3. Map protocol settings file to study
--------------------------------------
See config_template.json for examples. This involves simply adding a line in your
config.json to map MY_STUDY_NAME to my_protocol_file.sh.
