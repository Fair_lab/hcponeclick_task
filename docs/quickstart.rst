Quickstart
==========

Limitations
-----------

#. Any subject data processed through the current version of this pipeline must include the following modalities:
    
    #. T1
    #. T2
    #. DFM phase and magnitude
    #. EPI(s)

Prerequisites
-------------
#. User must have an exacloud account. Contact earl@ohsu.edu for information about how to get an exacloud account.
#. User must be added to the fnl_lab exacloud linux group. Email acc@ohsu.edu to fulfill this request, 
    and cc earl@ohsu.edu so he can verify that the Fair lab wants you to have access.

Set Up via PATHGUI
------------------

#. launch the release version of the PATHGUI: :code:`/group_shares/PSYCH/code/release/GUIs/PATH_GUI/PATH.pyw`
#. load 'EXA_HCP.config': select "file"->"Load .config"
#. Select your study name from the dropdown.*
#. Search for your subject and visit as usual, then select T1, T2, DFM(phase and mag), and EPIs**
#. Click :code:`Sort DICOM(s)`
#. Select 'EXA_HCP' as your pipeline
#. Click :code:`PROCESS and SeedCorrel`
#. Follow instructions to set up ssh keys, if necessary.


\* If you don't see your study in the dropdown list, follow the instructions here: :ref:`new-study-instructions`

\** For more information on how to use the PATH_GUI, see https://everett.ohsu.edu/wiki/PathGui

Manual Run from Exacloud
------------------------

For more experienced users, it is possible to start processing without using the PATH_GUI.
This requires extra setup by the user:

#. The subject's data must already be converted to niftis (and slicetime corrected, if desired).
#. These niftis must be placed on exacloud in a folder following the CYA and HCP format:

   :code:`../HCP/processed/STUDY/SUBJECTID/VISIT/HCP_release_20151207/unprocessed/NIFTIs/`

To launch processing, see: :ref:`run-oneclick`

Known Issues and Workarounds
============================
See https://trello.com/b/FR6S0dpf/exa-hcp for a list of logged bugs and feature requests, and feel free to log
new bugs in the 'Backlog' column or email a computing team member for more information.

#. AIRC setup does not currently support ssh keys that use passphrases. If you have such ssh keys already set up,
   you will need to stash these and let the PATHGUI set up new ssh keys for you. There is already a feature request
   logged for fixing this issue.
#. The HTCondor queueing system fails if you don't source /etc/bashrc. The ACC has been alerted to this issue, 
   and will fix it if it affects more people. In the meantime, please modify your startup files as described here:

    a. add to/create your ~/.bashrc

       .. code-block:: bash

           if [ -f /etc/bashrc ]; then
           . /etc/bashrc
           fi

    b. add to/create your ~/.bash_profile

       .. code-block:: bash

           if [ -f ~/.bashrc ]; then
           . ~/.bashrc
           fi

#. Python user package configuration on exacloud has been known to cause issues in the HCP pipeline.
   Sometimes, a user's Python configuration will cause an error with the FSL `aff2rigid` utility. 
   Until a better solution is found, the workaround for this is to temporarily stash the user's
   ~/.local folder by renaming it.
