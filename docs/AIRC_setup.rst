AIRC setup module
*****************
The AIRC setup module does just that--all of the AIRC setup tasks necessary to 
get data packaged and sent to exacloud.

setup_sshkeys.py
----------------
.. argparse::
   :module: AIRC_setup.setup_sshkeys
   :func: get_parser
   :prog: ./setup_sshkeys.py

EXA_HCP_wrapper.py
------------------
.. argparse::
   :module: AIRC_setup.EXA_HCP_wrapper
   :func: get_parser
   :prog: ./EXA_HCP_wrapper.py

exacloud_transfer.py
--------------------
.. argparse::
   :module: AIRC_setup.exacloud_transfer
   :func: get_parser
   :prog: ./exacloud_transfer.py