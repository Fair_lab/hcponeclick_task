Main Scripts on Exacloud
************************

.. _run-oneclick:

run_oneclick.py
---------------

.. argparse::
   :module: run_oneclick
   :func: get_parser
   :prog: ./run_oneclick.py

run_node.py
-----------

.. argparse::
   :module: run_node
   :func: get_parser
   :prog: ./run_node.py

temp