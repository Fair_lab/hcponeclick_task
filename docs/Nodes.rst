Nodes Module
************

NodeManager
-----------
.. automodule:: Nodes.NodeManager
    :special-members:
    :exclude-members: __weakref__
    :members:

NodeBase
--------
.. automodule:: Nodes.NodeBase
    :special-members:
    :exclude-members: __weakref__
    :members:

