from .NodeBase import NodeStep
from util import UtilityBelt
import subprocess
import os
from util import UtilityBelt

class HcpGenericWrapper(NodeStep):
    MemRequired = .5
    expected_outputs = ['{ID}/Scripts/PreFreeSurferPipelineBatch.sh',
                        '{ID}/Scripts/FreeSurferPipelineBatch.sh',
                        '{ID}/Scripts/PostFreeSurferPipelineBatch.sh',
                        '{ID}/Scripts/GenericfMRIVolumeProcessingPipelineBatch.sh',
                        '{ID}/Scripts/GenericfMRISurfaceProcessingPipelineBatch.sh',
                        '{ID}/Scripts/hcp_runner.sh',
                        '{ID}/Scripts/ProtocolSettings_copy.sh',
                        '{ID}/unprocessed/NIFTI/{ID}_T1w_MPR1.nii.gz']


    def run_command(self):
        #example: ./hcp_wrapper.sh ProtocolSettings_ADHD.sh FD0005 /home/exacloud/lustre1/users/waltonwe/HCP_test/OneClickTest/FD0005/20120925-SIEMENS_TrioTim-Nagel_K_Study/
        util = UtilityBelt()
        executable = util.get_path('Exacloud','hcp_wrapper')

        protocol = util.get_protocol(os.path.join(self.subject_path))
        print "protocol is:"
        print protocol

        print [executable, protocol, self.subjectID, self.subject_path]
        subprocess.call([executable, protocol, self.subjectID, self.subject_path])

    def get_failure_comment(self):
        return """Failure in this step indicates that something is wrong with your configuration.
                Please contact a member of the Fair Lab for more information"""

