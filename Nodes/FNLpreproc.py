from .NodeBase import NodeStep
import os
import subprocess


class FNLpreproc(NodeStep):

    expected_outputs = ['{ID}/summary_FNL_preproc/all_FD.txt',
                        '{ID}/summary_FNL_preproc/{ID}_*REST*_in_t1.gif',
                        '{ID}/summary_FNL_preproc/DVARS_and_FD_CONCA.png',
                        '{ID}/FNL_preproc/analyses_v2/timecourses/Gordon_subcortical.csv',
                        '{ID}/FNL_preproc/analyses_v2/timecourses/Gordon.csv',
                        '{ID}/FNL_preproc/analyses_v2/timecourses/Power.csv',
                        '{ID}/FNL_preproc/analyses_v2/timecourses/Yeo.csv',
                        '{ID}/FNL_preproc/analyses_v2/matlab_code/FD.mat',
                        '{ID}/FNL_preproc/analyses_v2/matlab_code/motion_numbers.mat',
                        '{ID}/FNL_preproc/analyses_v2/matlab_code/power_2014_motion.mat']
    MemRequired = 4

    def run_command(self):

        executable = '/home/exacloud/lustre1/fnl_lab/projects/earl/FNL_preproc/FNL_preproc_wrapper.sh'
        output_fold = os.path.join(self.subject_path, self.subjectID)

        analysis_folder = self.__get_analyses_folder()

        cmdlist = [executable, self.subjectID, output_fold, analysis_folder]
        print cmdlist
        subprocess.call(cmdlist)

    def __get_analyses_folder(self):

        HCP_end_index = self.subject_path.find('HCP') + 3
        HCP_dir = self.subject_path[:HCP_end_index]

        split_path = self.subject_path.split('/')

        vis = split_path[-1]
        study_name = split_path[-4]
        #control for trailing slash        
        if not vis:
            vis = split_path[-2]
            study_name = split_path[-5]

        return os.path.join(HCP_dir,'analyses_v2', study_name, self.subjectID + '+' + vis)
