from .NodeBase import NodeStep
import os,re
from glob import glob
import subprocess
from util import UtilityBelt

# CHANGELOG
# Anders Perrone - 11/29/2016 - Changed "--Subjlist" to "--Subject" in accordance with the new pipeline batch scripts
# Eric Earl      - 12/27/2016 - Updates to allow "REST" scans to be out of chronological order
# Eric Earl      - 07/31/2017 - Updates for an "rfMRI_" prefix to allow for both "rfMRI_REST" and "REST" instead of just "REST"
# Eric Earl      - 08/01/2017 - Update to try displaying the command before running it

class HcpVol(NodeStep):

    expected_outputs = ['{ID}/MNINonLinear/Results/*REST1/*REST1.nii.gz']
    max_runs = 4

    def get_failure_comment(self):
        return 'Failed after 4 trials. This is usually due to issues creating the movement regressers file, which cannot be resolved at this time.'

    def run_command(self):
        executable = os.path.join(self.subject_path,self.subjectID,'Scripts/GenericfMRIVolumeProcessingPipelineBatch.sh')

        cmdlist = [executable, '--runlocal', '--Subject='+self.subjectID, '--StudyFolder='+self.subject_path]
        subprocess.call(cmdlist)

    def succeeded(self):
        if not NodeStep.succeeded(self):
            return False
        #check movement regressors file
        return self.movement_reg_isvalid()

    def movement_reg_isvalid(self):
        mov_reg_string = os.path.join(self.subject_path, self.subjectID, 'MNINonLinear/Results/{}REST{}/Movement_Regressors.txt') # edited by Eric Earl 07/31/2017

        pattern = re.compile('.+_REST([0-9]+)\.nii\.gz$') # added by Eric Earl 12/27/2016
        rest_match = os.path.join(self.subject_path, self.subjectID, 'unprocessed/NIFTI/*_REST*.nii.gz') # edited by Eric Earl 12/27/2016

        for REST in glob(rest_match): # edited by Eric Earl 12/27/2016
            try:
                if 'rfMRI_' in REST:  # added by Eric Earl 07/31/2017
                    prefix = 'rfMRI_' # added by Eric Earl 07/31/2017
                else:                 # added by Eric Earl 07/31/2017
                    prefix = ''       # added by Eric Earl 07/31/2017
                rest_num = pattern.match(REST).group(1) # added by Eric Earl 12/27/2016
                mov_reg_path = UtilityBelt().get_path('Exacloud','mov_reg_path')
                validity_command_list = [mov_reg_path, '--fmri', REST, '--movmnt',mov_reg_string.format(prefix,rest_num)] # added by Eric Earl 08/01/2017
                print validity_command_list                                                                               # added by Eric Earl 08/01/2017
                validity = subprocess.check_output(validity_command_list)                                                 # edited by Eric Earl 08/01/2017
                if not validity.strip() == 'valid':
                    return False
            except Exception as e:
                print e
                return False

        return True


