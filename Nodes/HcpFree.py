from .NodeBase import NodeStep
import os
from shutil import rmtree
import subprocess

class HcpFree(NodeStep):
    #########################NoT2 edits: nii.gz->mgz and removed white.nii.gz -AndersPerrone
    expected_outputs = ['{ID}/T1w/{ID}/mri/lh.ribbon.mgz',
                        '{ID}/T1w/{ID}/mri/rh.ribbon.mgz',
                        '{ID}/T1w/{ID}/mri/T1w_hires.nii.gz',
                        '{ID}/T1w/T1w_acpc_dc_restore_brain_1mm.nii.gz']
#                        '{ID}/T1w/{ID}/mri/white.nii.gz']
    max_runs = 20
    job_length = 'LONG'
    MemRequired = 10

    def get_failure_comment(self):
        return 'Failed after two trials. This is usually due to poor image quality which results in inability of recon-all to succeed (cortical reconstruction)'

    def run_command(self):
        #remove freesurfer directory--necessary when freesurfer is being rerun
        freesurfer_path = os.path.join(self.subject_path, self.subjectID, 'T1w', self.subjectID)
        if os.path.exists(freesurfer_path):
            rmtree(freesurfer_path)

        executable = os.path.join(self.subject_path,self.subjectID,'Scripts/FreeSurferPipelineBatch.sh')

        cmdlist = [executable, '--runlocal', '--Subject='+self.subjectID, '--StudyFolder='+self.subject_path]
        print cmdlist
        subprocess.call(cmdlist)

#Changed --Subjlist to --Subject in accordance with the new pipeline batch scripts - Anders Perrone 20161129



