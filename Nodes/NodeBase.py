"""
.. module:: NodeBase
    :platform: Unix
    :synopsis: Foundation module for each node (step) in the pipeline. Handles all common node functionality.

"""

# CHANGELOG
# Eric Earl - 07/31/2017 - Imported glob to be used instead of os.path.exists for checking existence of "expected_outputs"

import os, json, subprocess,sys
from util import UtilityBelt
from glob import glob

class StatusVal(object):
    not_started = 4
    incomplete = 2
    failed = 3
    succeded = 1

class Status(object):
    """Status provides and updates node status information.

    Status information for each node is stored in the hcponeclick/NodeName/status.json file. 
    This class provides an abstraction layer between the NodeStep class and this status file.

    """
    name = "status.json"
    def __init__(self,folder_path):
        """
        Args:
            folder_path (str): absolute path to the node's bookkeeping folder (e.g. /data/MySubj/MyVis/hcponeclick/NodeStep)
        """
        self.file_path = os.path.join(folder_path, Status.name)

        if os.path.exists(self.file_path):
            self.load_status()
        else:
            self.num_runs = 0
            self.node_status = StatusVal.not_started
            self.comment = ''
            self.write_status()


    def load_status(self):
        """parse status.json, loading num_runs and node_status into the instance
        """
        with open(self.file_path,'r') as f:
            status_json = json.load(f)
            self.num_runs = int(status_json['num_runs'])
            self.node_status = status_json['node_status']
            self.comment = status_json['comment']

            f.close()

        return self
    
    def write_status(self):
        """write instance's status to the status.json file.
        """
        status_obj = {'num_runs':self.num_runs,'node_status': self.node_status, 'comment': self.comment}
        f=open(self.file_path,'w+')
        json.dump(status_obj,f, indent=4)
        f.close()

    def increment_run(self):
        if self.num_runs:
            self.num_runs += 1
        else:
            self.num_runs = 1

    def update_start_run(self):
        self.increment_run()
        self.node_status = StatusVal.incomplete
        self.write_status()

    def update_success(self):
        self.node_status = StatusVal.succeded
        self.write_status()

    def update_failure(self, comment=''):
        self.node_status = StatusVal.failed
        self.comment = comment
        self.write_status()

    def succeded(self):
        return self.node_status == StatusVal.succeded



class NodeStep(object):
    """ Base class for all hcponeclick nodes.

    Handles running commands, creating creating condor_submit files, and determining
    success or failure of a processing step.
    """
    expected_outputs = []
    max_runs = 1 # the number of times a node should be run before pipeline fails
    job_length = 'SHORT'
    queuing_args = []
    status = None
    MemRequired = 2 # memory required to run this node, in GB


    def __init__(self, subjectID, subject_path):
        """
        Args:
            subjectID (str): subject ID, should be the same as the name of the subject folder
            subject_path (str): absolute path to subject's processed pipeline folder
        """
        self.subjectID = subjectID
        self.subject_path = subject_path
        self.node_folder = os.path.join(self.subject_path,'hcponeclick_task',self.get_classname())
        self.num_runs = 0
        

    def get_classname(self):
        return self.__class__.__name__

    def run(self):
        """ Main method for running the node NodeStep

        Routes to other functions for updating the status, running the processing 
        step on the command line, and reporting success or failure.
        This is the routing method that is called by the nodemanager in the queue
        """
        if not os.path.exists(self.node_folder):
            os.makedirs(self.node_folder)

        self.status = self.get_status()

        self.status.update_start_run()

        self.run_command()

        self.__update_permissions()

        if self.succeeded():
            self.status.update_success()
        else: 
            self.status.update_failure(self.get_failure_comment())


    def run_command(self):
        """ Run shell commands for node.

        This should be overridden in by each child class to run specific commands for that node.
        If not implemented by child, prints a warning and creates mock outputs.
        """
        print "Run command not implemented by node step. Simply creating testing outputs"
        self.mock_expected_outputs()


    def succeeded(self):
        """ Determines the success or failure of a node run

        Base implementation checks to make sure all expected_outputs have been created.
        See Nodes.HcpVol class for an example of overriding this behavior to add more specialized checked
        """
        if not self.expected_outputs:
            return True

        #check all file outputs for existence
        for f_path in self.expected_outputs:
            subj_f_path = f_path.replace('{ID}', self.subjectID)
            output_path = os.path.join(self.subject_path, subj_f_path)
            # edited by Eric Earl 07/31/2017
            if glob(output_path) == []:
                return False

        return True


    def __update_permissions(self):
        """
        Grant rw privileges to the group and user.
        """
        chmod_cmd = "chmod -R ug+rw {}".format(self.subject_path)
        subprocess.call(chmod_cmd, shell=True)


    def get_status(self):
        if not self.status:
            self.status = Status(self.node_folder)

        return self.status


    def should_rerun(self):
        if self.status.num_runs < self.max_runs:
            return True

        return False


    def get_failure_comment(self):
        return ''


    def queue(self):
        """ Create submit file and submits it to the condor queue.
        """
        if not os.path.exists(self.node_folder):
            os.makedirs(self.node_folder)

        prog_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        run_node_path = os.path.join(prog_dir, 'run_node.py')
        
        #create queue file
        submit_file = os.path.join(self.node_folder, 'submit_{}')
        submit_file = submit_file.format(self.get_classname())
        Shortname = self.get_classname()
        condor_maker_path = UtilityBelt().get_path('Exacloud','condor_maker_path')

        create_submitfile_cmd = [condor_maker_path, submit_file, Shortname, run_node_path, str(self.MemRequired), self.job_length]
        args = ['-id',self.subjectID, '-s', self.subject_path, '-node', self.get_classname()]
        create_submitfile_cmd.extend(args)
        print create_submitfile_cmd
        subprocess.call(create_submitfile_cmd)

        #queue the submit file from the scheduling node
        if os.uname()[1] == 'exalab3.ohsu.edu':
            submit_cmd = 'condor_submit {}'.format(submit_file)
        else:
            submit_cmd = 'ssh exalab3 "condor_submit {}"'.format(submit_file)

        print submit_cmd
        subprocess.call(submit_cmd, shell=True)


    def mock_expected_outputs(self):
        """ poor man's testing method. Useful when prototyping new nodes.
        """
        for partial_path in self.expected_outputs:
            subj_f_path = partial_path.replace('{ID}', self.subjectID)
            
            file_path = os.path.join(self.subject_path,subj_f_path)
            
            if not os.path.exists(os.path.dirname(file_path)):
                os.makedirs(os.path.dirname(file_path))
                
            with open(file_path, "w") as f:
                f.write("Testing output generated by hcponeclick_task")
                f.close()

