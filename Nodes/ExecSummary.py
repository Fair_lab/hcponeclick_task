from .NodeBase import NodeStep
import os
import subprocess


class ExecSummary(NodeStep):

    expected_outputs = ['']
    MemRequired = 4

    def run_command(self):

        executable = '/home/exacloud/lustre1/fnl_lab/code/executivesummary/summary_tools/layout_builder.py'
        subject_folder = os.path.join(self.subject_path, self.subjectID)
        output_folder = os.path.join(self.study_path)


        cmdlist = [python, executable, '-o', output_folder, '-s', subject_folder]
        print cmdlist
        subprocess.call(cmdlist)

    def get_failure_comment(self):
        return """Failure in this step indicates that Anders is a useless programmer and can't do anything right.""""


    def __get_analyses_folder(self):

        HCP_end_index = self.subject_path.find('HCP') + 3
        HCP_dir = self.subject_path[:HCP_end_index]

        split_path = self.subject_path.split('/')

        vis = split_path[-1]
        study_name = split_path[-4]
        #control for trailing slash        
        if not vis:
            vis = split_path[-2]
            study_name = split_path[-5]

        return os.path.join(HCP_dir,'analyses_v2', study_name, self.subjectID + '+' + vis)
