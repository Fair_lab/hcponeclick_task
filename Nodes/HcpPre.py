from .NodeBase import NodeStep
import os,subprocess

class HcpPre(NodeStep):

    expected_outputs = ['{ID}/MNINonLinear/T1w_restore.nii.gz',
                        '{ID}/T1w/T1w_acpc_dc_restore_brain.nii.gz',
                        '{ID}/MNINonLinear/T1w_restore_brain.nii.gz',
                        #'{ID}/T1w/T2w_acpc_dc_restore_brain.nii.gz',
                        #'{ID}/MNINonLinear/T2w_restore.nii.gz',
                        #'{ID}/MNINonLinear/T2w_restore_brain.nii.gz' commented out for NoT2 - Anders Perrone 11/3/2016
                        ]

    def run_command(self):
        executable = os.path.join(self.subject_path,self.subjectID,'Scripts/PreFreeSurferPipelineBatch.sh')

        print [executable, '--runlocal', '--Subject='+self.subjectID, '--StudyFolder='+self.subject_path]
        subprocess.call([executable, '--runlocal', '--Subject='+self.subjectID, '--StudyFolder='+self.subject_path])


#Changed --Subjlist to --Subject in accordance with the new pipeline batch scripts - Anders Perrone 20161129
