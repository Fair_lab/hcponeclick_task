from .NodeBase import NodeStep
import os
import subprocess

# CHANGELOG
# Anders Perrone - 11/29/2016 - Changed "--Subjlist" to "--Subject" in accordance with the new pipeline batch scripts 
# Eric Earl      - 07/31/2017 - Edited top 4 expected_outputs since the folders can be called either "rfMRI_REST" or "REST" now

class HcpSurf(NodeStep):

    # edited by Eric Earl 07/31/2017
    expected_outputs = ['{ID}/MNINonLinear/Results/*REST1/*REST1.nii.gz',
                        '{ID}/MNINonLinear/Results/*REST1/*REST1_Atlas.dtseries.nii',
                        '{ID}/MNINonLinear/Results/*REST1/*REST1.L.atlasroi.32k_fs_LR.func.gii',
                        '{ID}/MNINonLinear/Results/*REST1/*REST1.R.atlasroi.32k_fs_LR.func.gii',
                        '{ID}/MNINonLinear/ROIs/Atlas_ROIs.2.nii.gz',
                        '{ID}/MNINonLinear/ROIs/ROIs.2.nii.gz']

    def run_command(self):
        executable = os.path.join(self.subject_path,self.subjectID,'Scripts/GenericfMRISurfaceProcessingPipelineBatch.sh')

        cmdlist = [executable, '--runlocal', '--Subject='+self.subjectID, '--StudyFolder='+self.subject_path] # edited by Anders Perrone 11/29/2016
        subprocess.call(cmdlist)


