from . import (HcpGenericWrapper, HcpPre, HcpFree, HcpPost, HcpVol, HcpSurf, FNLpreproc, FNLpreproc_v2)
from Exceptions import NodeNotFoundException
import subprocess, os, pwd

class NodeManager(object):
    """
    NodeManager handles how all nodes interact with eachother and outward-facing scripts

    Called by run_node.py and run_oneclick.py. Figures out which node to run, and what to
    do on success or failure of a node.
    """

    def __init__(self, subjectID, subject_path):
        """
        Args:
            subjectID (str): subject ID, should be the same as the name of the subject folder
            subject_path (str): absolute path to subject's processed pipeline folder
        """
        self.subjectID = subjectID
        self.subject_path = subject_path
        self._nodes = None


    @property
    def nodes(self):
        return self._nodes

    @nodes.getter
    def nodes(self):
        """
        Get or create a nodes, the ordered list of NodeStep instances
        """
        if not self._nodes:
            self._nodes = [HcpGenericWrapper(self.subjectID, self.subject_path),
                        HcpPre(self.subjectID, self.subject_path),
                        HcpFree(self.subjectID, self.subject_path),
                        HcpPost(self.subjectID, self.subject_path),
                        HcpVol(self.subjectID, self.subject_path),
                        HcpSurf(self.subjectID, self.subject_path),
                        FNLpreproc_v2(self.subjectID, self.subject_path)]
#                        ExecSumm(self.subjectID, self.subject_path), CustomClean(self.subjectID, self.subject_path)]

        return self._nodes

    def first_node_name(self):
        return self.nodes[0].get_classname()

    def get_next_node(self, latest):
        """
        Args:
            latest (NodeStep): the current node
        """
        next_index = None

        for i,node in enumerate(self.nodes):
            if node.get_classname() == latest.get_classname():
                next_index = i+1
                break

        if not next_index:
            exception =  NodeNotFoundException(latest.get_classname())

            raise exception

        elif next_index > len(self.nodes)-1:
            return None

        return self.nodes[next_index]

    def get_node(self,node_name):
        """Get a node instance given a NodeStep classname

        Args:
            node_name(str): class name of the desired node

        Returns:
            NodeStep: the NodeStep instance indicated by node_name
        """
        print node_name
        for node in self.nodes:
            if node.get_classname() == node_name:
                return node
        exception =  NodeNotFoundException(node_name)

        raise exception
# Added by Anders 20170801 to test if node ran succesfully without actually running the node
    def test_run_node(self,node_name):
        """ get the desired node and call its run method

        Args:
            node_name(str): class name of the desired node
        """
        curr_node = self.get_node(node_name)
        #curr_node.run()
        self.handle_node_ended(curr_node)
    
    def run_node(self,node_name):
        """ get the desired node and call its run method

        Args:
            node_name(str): class name of the desired node
        """
        curr_node = self.get_node(node_name)
        curr_node.run()
        self.handle_node_ended(curr_node)

    def queue_node(self,node_name):
        """add node to the server's job queue

        Args:
            node_name(str): class name of the desired node
        """
        curr_node = self.get_node(node_name)
        curr_node.queue()

    def handle_node_ended(self,node):
        """ handle success or failure of a NodeStep

        In response to success/failure of node, queues the next node, sends failure message
        or sends success message

        Args:
            node(NodeStep): instance of the node that has just finished
        """
        if not node.succeeded():
            if node.should_rerun():
                print "rerunning node {}".format(node.get_classname())
                self.run_node(node.get_classname())
            else:
                print "failing on node {}".format(node.get_classname())
                self.send_failure(node)
        else:
            print "node {} succeeded".format(node.get_classname())
            next_node = self.get_next_node(node)
            if next_node:
                print "queueing node {} for first run".format(next_node.get_classname())
                next_node.queue()
            else:
                print "finished on node {}".format(node.get_classname())
                self.send_success(node)


    def send_success(self,node):
        msg = "Processing succeeded!\nYou can find your data for subject {} at:\n{}".format(node.subjectID, node.subject_path)
        self.mail(msg)

    def send_failure(self,node):
        msg = "Processing failed for subject {} during node step {}\n\n"
        msg = msg.format(node.subjectID, node.get_classname())

        if node.get_failure_comment():
            msg = msg + "Failure Details:\n{}".format(node.get_failure_comment())
        self.mail(msg)

    def mail(self, msg):
        """ Use linux 'mail' utility to send email message to the user

        Args:
            msg(str): msg to send
        """
        subj = '"[Exacloud HCP] Processing Update for {}"'.format(self.subjectID)
        msg = '"{}"'.format(msg)
        usr = pwd.getpwuid(os.getuid()).pw_name
        cmd = 'echo {} | mail -s {} {}@ohsu.edu'.format(msg,subj,usr)

        print cmd
        subprocess.call(cmd, shell=True)
