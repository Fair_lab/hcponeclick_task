from .NodeBase import NodeStep
import os
import subprocess

class HcpPost(NodeStep):

    expected_outputs = ['{ID}/T1w/ribbon.nii.gz',
                        '{ID}/MNINonLinear/ribbon.nii.gz',
                        '{ID}/MNINonLinear/fsaverage_LR32k/{ID}.ArealDistortion_FS.32k_fs_LR.dscalar.nii',
                        '{ID}/MNINonLinear/{ID}.thickness.164k_fs_LR.dscalar.nii',
                        '{ID}/MNINonLinear/{ID}.sulc.164k_fs_LR.dscalar.nii',
                        '{ID}/MNINonLinear/{ID}.curvature.164k_fs_LR.dscalar.nii',
                        '{ID}/MNINonLinear/{ID}.ArealDistortion_FS.164k_fs_LR.dscalar.nii',
                        '{ID}/MNINonLinear/fsaverage_LR32k/{ID}.thickness.32k_fs_LR.dscalar.nii',
                        '{ID}/MNINonLinear/fsaverage_LR32k/{ID}.sulc.32k_fs_LR.dscalar.nii',
                        '{ID}/MNINonLinear/fsaverage_LR32k/{ID}.curvature.32k_fs_LR.dscalar.nii']
    max_runs = 1

    def run_command(self):
        executable = os.path.join(self.subject_path,self.subjectID,'Scripts/PostFreeSurferPipelineBatch.sh')

        cmdlist = [executable, '--runlocal', '--Subject='+self.subjectID, '--StudyFolder='+self.subject_path]
        print cmdlist
        subprocess.call(cmdlist)

#Changed --Subjlist to --Subject in accordance with the new pipeline batch scripts
