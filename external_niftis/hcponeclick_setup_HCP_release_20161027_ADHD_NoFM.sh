#! /bin/bash

NIFTIDIR=$1
SUBJECT=$2
VISIT=$3
STUDYNAME="ADHD_NoFM"
PROTOCOL_SH_NAME="ADHD_NoFM_protocol.sh"
PROCESSEDDIR="/home/exacloud/lustre1/fnl_lab/data/HCP/processed"

`dirname $0`/hcponeclick_setup.sh $NIFTIDIR $SUBJECT $VISIT $STUDYNAME $PROTOCOL_SH_NAME $PROCESSEDDIR

