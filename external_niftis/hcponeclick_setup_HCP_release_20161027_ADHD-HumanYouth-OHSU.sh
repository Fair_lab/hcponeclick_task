#!/bin/bash

NIFTIDIR=$1 # A directory of ONLY HCP-named-and-ready NIFTIs, no other files
SUBJECT=$2 # Subject ID folder name
VISIT=$3 # VISIT folder name
STUDYNAME="ADHD-HumanYouth-OHSU"
PROTOCOL_SH_NAME="RF_ASD_KSADS_protocol.sh"
PROCESSEDDIR="/home/exacloud/lustre1/fnl_lab/data/HCP/processed"

`dirname $0`/hcponeclick_setup.sh $NIFTIDIR $SUBJECT $VISIT $STUDYNAME $PROTOCOL_SH_NAME $PROCESSEDDIR

