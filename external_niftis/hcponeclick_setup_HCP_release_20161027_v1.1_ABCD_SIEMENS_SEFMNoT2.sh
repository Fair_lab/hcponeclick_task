#! /bin/bash

NIFTIDIR=$1
SUBJECT=$2
VISIT=$3
STUDYNAME="ABCD_SIEMENS_SEFMNoT2"
PROTOCOL_SH_NAME="ABCD_SIEMENS_SEFMNoT2_protocol_HCP_release_20161027_v1.1.sh"
PROCESSEDDIR="/home/exacloud/lustre1/fnl_lab/data/HCP/processed"

`dirname $0`/hcponeclick_setup.sh $NIFTIDIR $SUBJECT $VISIT $STUDYNAME $PROTOCOL_SH_NAME $PROCESSEDDIR

