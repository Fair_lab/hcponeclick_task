#!/bin/bash

# This script requires NO TYPOS and that the NIFTI files in ${NIFTIDIR} are already named as "${SUBJECT}_${SCAN}${NUMBER}.nii.gz", e.g. MYSUBJ_REST1.nii.gz

NIFTIDIR=$1
SUBJECT=$2
VISIT=$3
STUDYNAME=$4
PROTOCOL_SH_NAME=$5
PROCESSEDDIR=$6
VISITDIR=${PROCESSEDDIR}/${STUDYNAME}/${SUBJECT}/${VISIT}

### RUN THE ORIG_PROTOCOL_SH ###
ORIG_PROTOCOL_SH=`dirname ${0}`/../protocols/${PROTOCOL_SH_NAME}
if [ -e ${ORIG_PROTOCOL_SH} ] ; then
    . ${ORIG_PROTOCOL_SH}
else
    echo ${ORIG_PROTOCOL_SH}' does not exist!  Quitting.'
    exit 1
fi

PIPENAME=`basename ${HCPPIPEDIR}`

### RUN THE HCP WRAPPER TO CREATE SCRIPTS ###
. /home/exacloud/lustre1/fnl_lab/code/internal/pipelines/HCP_generic/hcp_wrapper_task.sh ${ORIG_PROTOCOL_SH} ${SUBJECT} ${VISITDIR}/${PIPENAME}

UNPROCDIR=${PROCDIR}/unprocessed/NIFTI
EPRIMEDIR=${PROCDIR}/unprocessed/EPRIME
HCPONECLICKDIR=${PIPEDIR}/hcponeclick_task

rm -rf ${UNPROCDIR} ${EPRIMEDIR} ${PROCDIR}/REST* ${PROCDIR}/MNINonLinear/Results/REST*

#check if unprocessed and hcponeclick folders have already been created
if [ ! -d ${UNPROCDIR} ]; then
    mkdir -p ${UNPROCDIR}
fi
if [ ! -d ${EPRIMEDIR} ]; then
    mkdir -p ${EPRIMEDIR}
fi
if [ ! -d ${HCPONECLICKDIR} ]; then
    mkdir -p ${HCPONECLICKDIR}
fi

chown -R `whoami`:fnl_lab ${PIPEDIR}
chmod -R 770 ${PIPEDIR}

#make Scripts and hcponeclick directories
cp -R `dirname $0`/hcponeclick_template/* ${HCPONECLICKDIR}/

#Symbolic-Link NIFTIs to the subject's directory if it's not already there
if [ ! `readlink -f ${NIFTIDIR}` -ef `readlink -f ${UNPROCDIR}` ] ; then
    ln -s ${NIFTIDIR}/*.nii.gz ${UNPROCDIR}/
fi
cp -r ${NIFTIDIR}/../eprime/* ${EPRIMEDIR}/

#change path to log,err,out, and submit files in the subject's hcponeclick directory
sed -i "s|\.\.\./HCP_release_20161027|${PIPEDIR}|g" ${HCPONECLICKDIR}/*/submit*
sed -i "s|SUBJECT|${SUBJECT}|g" ${HCPONECLICKDIR}/*/submit*

