#! /bin/bash

NIFTIDIR=$1
SUBJECT=$2
VISIT=$3
STUDYNAME="ADHD_NoFMNoT2"
PROTOCOL_SH_NAME="ADHD_NoFMNoT2_protocol.sh"
PROCESSEDDIR="/home/exacloud/lustre1/fnl_lab/data/HCP/processed"

`dirname $0`/hcponeclick_setup.sh $NIFTIDIR $SUBJECT $VISIT $STUDYNAME $PROTOCOL_SH_NAME $PROCESSEDDIR

