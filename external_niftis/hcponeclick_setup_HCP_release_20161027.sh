#!/bin/bash

NIFTIDIR=$1
SUBJECT=$2
VISIT=$3
STUDYNAME=$4
PROTOCOL_SH_NAME=$5
PROCESSEDDIR="/home/exacloud/lustre1/fnl_lab/data/HCP/processed"

`dirname $0`/hcponeclick_setup.sh $NIFTIDIR $SUBJECT $VISIT $STUDYNAME $PROTOCOL_SH_NAME $PROCESSEDDIR

