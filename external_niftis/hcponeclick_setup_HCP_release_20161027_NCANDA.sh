#! /bin/bash

NIFTIDIR=$1
SUBJECT=$2
VISIT=$3
STUDYNAME="NCANDA-OHSU"
PROTOCOL_SH_NAME="NCANDA_protocol_HCP_release_20161027.sh"
PROCESSEDDIR="/home/exacloud/lustre1/fnl_lab/data/HCP/processed"

`dirname $0`/hcponeclick_setup.sh $NIFTIDIR $SUBJECT $VISIT $STUDYNAME $PROTOCOL_SH_NAME $PROCESSEDDIR

