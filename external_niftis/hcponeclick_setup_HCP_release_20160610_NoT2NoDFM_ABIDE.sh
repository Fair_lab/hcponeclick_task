#!/bin/bash

NIFTIDIR=$1
SUBJECT=$2
VISIT=$3
STUDYNAME="ABIDE"
PROTOCOL_SH_NAME="ABIDE_protocol.sh"
PROCESSEDDIR="/home/exacloud/lustre1/fnl_lab/data/HCP/processed"

`dirname $0`/hcponeclick_setup.sh $NIFTIDIR $SUBJECT $VISIT $STUDYNAME $PROTOCOL_SH_NAME $PROCESSEDDIR

